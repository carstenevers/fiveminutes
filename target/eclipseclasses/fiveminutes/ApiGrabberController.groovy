package fiveminutes
import grails.converters.JSON
import groovy.json.JsonSlurper


class ApiGrabberController {

    def index() { 
    	def apicall = new URL("https://api.spotify.com/v1/search?query=rock&offset=0&limit=20&type=track").getText()
    	def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
    	render result
    	
    }
    
    def genres(){
    	def apicall = new URL("https://api.spotify.com/v1/search?offset=0&limit=20&type=genre").getText()
    	def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
    	render result
    }
    
    def artistsByGenre(String genre) { 
    	def apicall = new URL("https://api.spotify.com/v1/search?query=genre%3A%22${genre}%22&offset=0&limit=20&type=artist").getText()
    	def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
    	render result
    	
    }
    
}


