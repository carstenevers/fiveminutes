<?php

    define('CACHE_FILE', '/tmp/cache.json');

    header('Content-type: application/json');

    if (file_exists(CACHE_FILE))
    {
        die(file_get_contents(CACHE_FILE));
    }

    $buffer = file_get_contents('http://52.18.46.242:8080/fiveminutes/apiGrabber/genre');

    file_put_contents(CACHE_FILE, $buffer);

    die($buffer);