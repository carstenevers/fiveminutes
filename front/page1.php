<?php

    $categories = ['rap', 'rnb', 'rock', 'classic'];
    $results = [];

    foreach($categories as $category)
    {
        $buffer = json_decode(file_get_contents('https://api.qwant.com/api/search/images?limit=1&q=' . $category), true);

        $results[] = [
            'title' => $category,
            'image' => $buffer['data']['result']['items'][0]['media'],
            'artists' => [
                [
                    'title' => 'Rihana',
                    'image' => '//s.qwant.com/thumbr/337x260/3/1/9685b897f8a12d03a3e5af8ae534f2/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.worldwideticketing.com%2Fimages%2Fspecial-events%2Frihanna%2Frihanna3.jpg&q=0&b=1&p=0'
                ],
                [
                    'title' => 'Rihana',
                    'image' => '//s.qwant.com/thumbr/337x260/3/1/9685b897f8a12d03a3e5af8ae534f2/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.worldwideticketing.com%2Fimages%2Fspecial-events%2Frihanna%2Frihanna3.jpg&q=0&b=1&p=0'
                ],
                [
                    'title' => 'Rihana',
                    'image' => '//s.qwant.com/thumbr/337x260/3/1/9685b897f8a12d03a3e5af8ae534f2/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.worldwideticketing.com%2Fimages%2Fspecial-events%2Frihanna%2Frihanna3.jpg&q=0&b=1&p=0'
                ],
                [
                    'title' => 'Rihana',
                    'image' => '//s.qwant.com/thumbr/337x260/3/1/9685b897f8a12d03a3e5af8ae534f2/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.worldwideticketing.com%2Fimages%2Fspecial-events%2Frihanna%2Frihanna3.jpg&q=0&b=1&p=0'
                ],
                [
                    'title' => 'Rihana',
                    'image' => '//s.qwant.com/thumbr/337x260/3/1/9685b897f8a12d03a3e5af8ae534f2/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.worldwideticketing.com%2Fimages%2Fspecial-events%2Frihanna%2Frihanna3.jpg&q=0&b=1&p=0'
                ],
                [
                    'title' => 'Rihana',
                    'image' => '//s.qwant.com/thumbr/337x260/3/1/9685b897f8a12d03a3e5af8ae534f2/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.worldwideticketing.com%2Fimages%2Fspecial-events%2Frihanna%2Frihanna3.jpg&q=0&b=1&p=0'
                ]
            ]
        ];
    }

    header('Content-type: application/json');

    die(json_encode($results));