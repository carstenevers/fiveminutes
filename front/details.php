<?php

    include "vendor/autoload.php";

    define('QWANT_API_ROOT', 'https://api.qwant.com/api');

    $client = new \Guzzle\Http\Client();

    $queries = [];
    $results = [];

    foreach($_POST['q'] as $q)
    {
        $queries[] = $client->get(QWANT_API_ROOT . '/search/news?count=3&q=' . urlencode($q));
        $queries[] = $client->get(QWANT_API_ROOT . '/search/videos?count=3&q=' . urlencode($q));
        $queries[] = $client->get(QWANT_API_ROOT . '/search/social?count=3&q=' . urlencode($q));
        $queries[] = $client->get('http://ws.audioscrobbler.com/2.0/?method=artist.getevents&api_key=dce2f2fd531f5c26a3bef97542187c94&format=json&artist=' . urlencode($q));
        $queries[] = $client->get('http://api.deezer.com/search?q=artist:' . urlencode($q));
    }

    $responses = $client->send($queries);

    foreach ($responses as $response)
    {
        $data = json_decode($response->getBody(), true);

        if (isset($data['data']['result']['items']))
        {
            $items = $data['data']['result']['items'];
        }
        elseif (isset($data['events']['event']))
        {
            $items = $data['events']['event'];

            if (isset($items['id']))
            {
                $items = [$items];
            }
        }
        elseif (isset($data['data']))
        {
            $items = array_slice($data['data'], 0, 2);
        }
        else
        {
            continue;
        }

        $results = array_merge($results, $items);
    }

    header('Content-type: application/json; charset=utf8');

    shuffle($results);

    die(json_encode($results));