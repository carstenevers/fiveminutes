<?php

    define('QWANT_API_ROOT', 'https://api.qwant.com/api');

    $q = $_GET['q'];

    $webResults = callQwantSearchAPI('web', $q);

    header('Content-type: text/html; charset=utf8');

    foreach($webResults['data']['result']['items'] as $item)
    {
        echo '<div>';

        echo '<h3>' . $item['title'] . '</h3>';

        echo '<p>' . $item['desc'] . '</p>';

        echo '</div>';
    }


    function callQwantSearchAPI($domain, $q)
    {
        $buffer = file_get_contents(QWANT_API_ROOT . '/search/' . $domain . '?q=' . urlencode($q));

        return json_decode($buffer, true);
    }