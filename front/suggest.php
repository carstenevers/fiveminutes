<?php

    define('QWANT_API_ROOT', 'https://api.qwant.com/api');

    $q = $_GET['q'];

    $buffer = json_decode(file_get_contents(QWANT_API_ROOT . '/suggest?q=' . urlencode($q)), true);

    $items = [];

    foreach($buffer['data']['items'] as $item)
    {
        $items[] = $item['value'];
    }

    header('Content-type: application/json');

    die(json_encode($buffer['data']['items']));