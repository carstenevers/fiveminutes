$(function()
{
    var menuData = null;

    function loadWelcomeMenu()
    {
        var $menu = $("#welcome-menu");

        $.post('/menu.php', function(data)
        {
            var html = '';

            menuData = data;

            $menu.removeClass('loading');

            $.each(data, function(i, item)
            {
                html += '<a class="_item ' + item.genre + '" data-position="' + i + '" style="background-image: url(' + item.image + ')" href="#">';

                html += '<div class="_background" ></div>';

                html += '<span>' + item.genre + '</span>';

                html += '<i class="select ion-ios-arrow-right"></i></a>';
            });

            $menu.html(html);
        });
    }

    function loadArtistsMenu(genreIndex)
    {
        var $menu = $("#menu-artists"), genre = menuData[genreIndex];
        var html = '';

        window.genre = genre;

        html += '<div class="list">';

        $menu.removeClass('loading');

        $.each(genre.artists, function(i, artist)
        {
            html += '<a class="_item" href="#">';

            html += '<img src="' + artist.image + '" />';

            html += '<span>' + artist.artistName + '</span>';

            html += '<i class="ion-checkmark-round"></i></a>';
        });

        html += '</div>';

        $menu.html('<div class="inner">' + html + '</div>"').css('display', 'block');

        $('body').css('background-image', 'url(' + genre.image + ')');

        $('header').html('Please select some artists <a href="#" id="btnLoadData" class="btn btn-success pull-right"><i class="ion-chevron-right"></i></a>');
    }

    $(document).on('click', '#welcome-menu a._item', function()
    {
        $('#welcome-menu').remove();

        loadArtistsMenu($(this).data('position'));

        return false;
    });

    $(document).on('click', '#menu-artists a', function()
    {
        $(this).toggleClass('selected');

        var $this = $(this);

        $(this).addClass('fx');

        setTimeout(function()
        {
            $this.removeClass('fx');
        }, 500);

        //loadData();

        return false;
    });

    loadWelcomeMenu();

    var xhrData = null;

    function loadData()
    {
        var queries = [], html = '';

        $('body').css('background-image', 'none').css('background-color', '#eee');

        $('#menu-artists a.selected').each(function(i, item)
        {
            queries.push($(item).find('span').text());

            html += '<img class="avatar" src="' + $(item).find('img').attr('src') + '" />';
        });

        html += '<a id="btShowList" href="#" style="display: none"><i class="ion-clipboard"></i></a> <a id="btShowMap" href="#"><i class="ion-map"></i></a>';

        $('header').html(html).css('background-image', 'url(' + genre.image + ')');;

        $.post('/details.php', {'q': queries}, function (response)
        {
            var html = '', geoJson = [];

            xhrData = null;

            $.each(response, function (i, item)
            {
                if (item.hasOwnProperty('type') && item.type === 'twitter')
                {
                    html += renderSocial(item);
                }
                else if (item.hasOwnProperty('type') && item.type === 'video')
                {
                    html += renderVideo(item);
                }
                else if (item.hasOwnProperty('preview'))
                {
                    html += renderSound(item);
                }
                else if (item.hasOwnProperty('venue'))
                {
                    html += renderEvent(item);

                    var venue = item.venue;

                    if (venue.hasOwnProperty('location') && venue.location.hasOwnProperty('geo:point') && venue.location['geo:point']['geo:lat'] !== '')
                    {
                        var feature = {
                            "type": "Feature",
                            "geometry": {
                                "type": "Point",
                                "coordinates": [
                                    parseFloat(venue.location['geo:point']['geo:long']),
                                    parseFloat(venue.location['geo:point']['geo:lat'])
                                ]
                            },
                            "properties": {
                                "title": item.title,
                                "icon": {
                                    "iconSize": [40, 40], // size of the icon
                                    "html": '<img class="avatar" src="' + item.image[3]['#text'] + '" />',
                                    "className": ""
                                }
                            }
                        };
                        geoJson.push(feature);
                    }
                }
                else
                {
                    html += renderNew(item);
                }
            });

            $('#page2').html(html).find('img').on('load', function()
            {
               var w = this.naturalWidth;

                if (w < 58)
                {
                    $(this).css('margin-left', (58-w)/2 + 'px');
                }

                console.log(w);
            });

            L.mapbox.accessToken = 'pk.eyJ1IjoiZWJ1aWxkeSIsImEiOiIwZDlkMTQyNmViZmRhNWE3MjAzNTFiMjQxMjY4MTI3YiJ9.48yIDJF19p9cDakJUZKoXQ';

            var map = window.map = L.mapbox.map('map', 'mapbox.streets').setView([52.5059117,13.393269], 12);

            var myLayer = L.mapbox.featureLayer().addTo(map);
            myLayer.on('layeradd', function(e) {
                var marker = e.layer,
                    feature = marker.feature;

                marker.setIcon(L.divIcon(feature.properties.icon));
            });

            myLayer.setGeoJSON(geoJson);

            map.fitBounds(myLayer.getBounds());
        });

        $('html').addClass('overflow');


    }

    function renderNew(item)
    {
        var media = '';

        if (item.hasOwnProperty('media') && item.media.length > 0)
        {
            media = '<img src="' + item.media[0].url + '" />';
        }

        var extra = '<div class="extra">' + item.desc + '<p class="url">' + item.url + '</p></div>';

        return '<a class="list-item new" href="' + item.url + '"><i class="_label ion-clock"></i> ' + media + '<span class="title">' + item.title + '</span><span class="time">' + moment.unix(item.date).fromNow() + '</span>' + extra + '</a>';
    }

    function renderSocial(item)
    {
        var media = '';

        media = '<img src="' + item.img + '" />';

        var extra = '';

        return '<a class="list-item social" href="' + item.url + '"><i class="_label ion-social-twitter"></i> ' + media + '<span class="title">' + item.desc.replace(/<(?:.|\n)*?>/gm, '') + '</span><span class="time">' + moment.unix(item.date).fromNow() + '</span>' + extra + '</a>';
    }

    function renderVideo(item)
    {
        var media = '';

        media = '<img src="' + item.thumbnail + '" />';

        var extra = '<div class="extra">' + item.desc + '<iframe data-src="' + item.media + '"></iframe><p class="url">' + item.url + '</p></div>';

        return '<a class="list-item video" href="' + item.url + '"><i class="_label ion-android-film"></i> ' + media + '<span class="title">' + item.title + '</span><span class="time">' + moment.unix(item.date).fromNow() + '</span>' + extra + '</a>';
    }

   function renderEvent(item)
   {
       var media = '';

       media = '<img src="' + item.image[3]['#text'] + '" />';

       var extra = '<div class="extra">' + item.venue.name + '<br />' + item.venue.location.city + '</div>';

       return '<a class="list-item event" href="' + item.url + '"><i class="_label ion-calendar"></i> ' + media + '<span class="title">' + item.title + '</span><span class="time">' + moment(item.startDate).fromNow() + '</span>' + extra + '</a>';

   }

    function renderSound(item)
    {
        var media = '', title = item.artist.name + '<br />' + item.title + ' (' + item.album.title + ')';

        media = '<img src="' + item.album.cover + '" />';

        var extra = '<div class="extra"><audio controls><source src="' + item.preview + '" type="audio/mp3"></audio></div>';

        return '<a class="list-item sound" href="' + item.link + '"><i class="_label ion-music-note"></i> ' + media + '<span class="title">' + title + '</span><span class="time">' + item.duration + ' s</span>' + extra + '</a>';

    }

    $(document).on('click', 'a.list-item', function()
    {
        if ($(this).toggleClass('expand').hasClass('expand'))
        {
            if ($(this).hasClass('video'))
            {
                var $frame = $(this).find('iframe');

                $frame.attr('src', $frame.data('src'));
            }
        }

        return false;
    })
    .on('click', '#list-wrapper a', function()
    {
        $(this).toggleClass('disabled');

        return false;
    })
    .on('click', '#btnLoadData', function()
    {
        $("#welcome-menu-wrapper").fadeOut(300, function()
        {
            $("#welcome-menu-wrapper").remove();
        });

        loadData();

        return false;
    })
    .on('click', '#btShowMap', function()
    {
        $('#btShowMap').hide();
        $('#btShowList').show();

        $('#map').show();
        $('#page2').hide();

        map.invalidateSize();
    })
    .on('click', '#btShowList', function()
    {
        $('#btShowMap').show();
        $('#btShowList').hide();

        $('#map').hide();
        $('#page2').show();
    });
});