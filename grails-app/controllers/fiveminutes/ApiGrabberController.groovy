package fiveminutes

import grails.converters.JSON
import groovy.json.JsonSlurper

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;


class ApiGrabberController {

    def restService

    def index() { 
    	def apicall = new URL("https://api.spotify.com/v1/search?query=rock&offset=0&limit=20&type=track").getText()
    	def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
    	render result
    	
    }
    
    def genres(){
    	def apicall = new URL("https://api.spotify.com/v1/search?offset=0&limit=20&type=genre").getText()
    	def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
    	render result
    }
    
    def artistsByGenre(String genre) { 
    	def apicall = new URL("https://api.spotify.com/v1/search?query=genre%3A%22${genre}%22&offset=0&limit=20&type=artist").getText()
    	def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)

        def items = result.target.artists.items

    	render result
    }

    def artistsItemsByGenre(String genre) {
        def apicall = new URL("https://api.spotify.com/v1/search?query=genre%3A%22${genre}%22&offset=0&limit=20&type=artist").getText()
        def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
        def items = result.target.artists.items
        return items
    }


    def artists (String genre) {
        def apicall = new URL("https://api.spotify.com/v1/search?query=genre%3A%22${genre}%22&offset=0&limit=25&type=artist").getText()
        def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
        def items = result.target.artists.items

        def artists = []
        items.each {
            artists << [artistName: it?.name, image: it?.images?.first()?.url]
        }

        return artists
    }

    def genre () {
        def genres = [
                [ genre: "rap",
                       image: "/img/rap.jpg",
                       artists: artists("rap")
                ],
                [ genre: "rock",
                        image: "/img/rock.jpg",
                        artists: artists("rock")
                ],
                [ genre: "soul",
                        image: "/img/soul.jpg",
                        artists: artists("soul")
                ],
                [ genre: "metal",
                        image: "/img/metal.jpg",
                        artists: artists("metal")
                ],
                [ genre: "techno",
                        image: "/img/techno.jpg",
                        artists: artists("techno")
                ]

        ]
        render genres as JSON
    }


    def artistNews (String name) {
        def apicall = new URL("https://api.qwant.com/api/search/news?count=25&locale=en_gb&offset=10&q=${name}").getText()
        def slurper = new JsonSlurper()
        JSON result = slurper.parseText(apicall)
        List news = result.target.data.result.items

        List news_ = []

        news.each {
            news_ << [desc: it.desc, source: it.source, artist: "${name}", title: it.title, url: it.url, date: it.date, media: it.media]
        }

        return news_
    }

    def artistsNews = {

        List jsonNews = []

        params."names[]".each {
            jsonNews << artistNews(it as String)
        }

        def sortedNews = jsonNews.flatten().sort {it.date}.reverse()

        render sortedNews as JSON
    }

    def artistConcerts (String name) {
        def apicall = new URL("http://api.bandsintown.com/artists/${name}/events.json?app_id=hack5minits").getText()
        def slurper = new JsonSlurper()
        JSON result = slurper?.parseText(apicall)

        List jsonArticles = []

        try {
            result.target.each {
                jsonArticles << [datetime: it.datetime, country: it.venue.country, city: it.venue.city,location: it.venue.name, lat: it.venue.latitude, lng: it.venue.longitude]
            }
        } catch (Exception e) {
            println "hidden error"
        }

        //render jsonArticles as JSON
        return [name: name, concerts: jsonArticles]
    }

    def artistsConcerts = {

        List jsonNews = []

        params."names[]".each {
            jsonNews << artistConcerts(it as String)
        }

        render jsonNews as JSON
    }




    def pop () {
        def articles = restService.artistArticle("Queen")
        render articles
    }

}


